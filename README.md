LibreHealth Planet
==============

_We based this off of [OpenMRS' configuration](https://github.com/openmrs/openmrs-contrib-planet)._

-------------------------------------------------------------------------------

This repository is used to configure the [RSS](https://en.wikipedia.org/wiki/RSS) feeds for
[planet.librehealth.io](http://planet.librehealth.io/). The LibreHealth Community uses a feed aggregagator
hosted by [OSUOSL](https://osuosl.org/). When changes are pushed to the configuration in this
repository, they are automatically deployed by OSUOSL via Jenkins.

## Which feeds are included?

We include any RSS feeds within or outside the community with LibreHealth content. For blogs that may contain
non-LibreHealth content, we prefer to filter to LibreHealth-related posts (e.g., only posts categorized or tagged
as LibreHealth).

For example, we add blogs for [Google Summer of Code](https://summerofcode.withgoogle.com/) and blogs of
community members to the aggregator.

## How do I add my blog or a relevated feed?

Submit a merge request to this repository including the feed. Add it to the bottom of the list following
the same format – that is, the web addrsess of ATOM or RSS content in square braces followed by a line
with `name = ` and the title for the feed (e.g., the name of the author):

```
[https://rss.example.com/]
name = Title for the feed here
```

If you aren't sure how to submit a merge request with your feed, then ask for help on
[LibreHealth Forums](https://forums.librehealth.io) or within our [Chat](https://chat/librehealth.io).

## How to feeds get removed?

You can submit a merge request to remove a feed or ask someone in the community for help. Anyone may
remove their own blog if they wish and we may occasionally merge out entries that are no longer functional,
no longer active, or no longer relevant for the LibreHealth community.
